import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Homepage from './pages/Homepage';
import Userspage from './pages/Userspage';
import Menupage from './pages/Menupage';
import Loginpage from './pages/Loginpage';
import PesananPage from './pages/PesananPage';

export default function AppRoutes() {
    return (
        <Router>
            <Switch>
                <Route path="/menus" component={Menupage} exact></Route>
                <Route path="/users" component={Userspage} exact></Route>
                <Route path="/login" component={Loginpage} exact></Route>
                <Route path="/pesanan" component={PesananPage} exact></Route>
                <Route path="/" component={Homepage} exact></Route>
            </Switch>
        </Router>
    )
}