import { useEffect, useState } from 'react';

export default function useFromApi(
    actionCreator,
    dependencyList
) {
    const [loading, setLoading] = useState(false);
    const [done, setDone] = useState(false);
    const [data, setData] = useState();

    const url = 'localhost:8080';

    useEffect(() => {
        setLoading(true);
        setDone(false);
        fetch(url + actionCreator.url, {
            method: actionCreator.method,
            body: JSON.stringify(actionCreator.payload)
        })
        .then(response => response.json())
        .then(data => {
            setLoading(false);
            setDone(true);
            setData(data);
        });
    }, [...dependencyList]);

    return { loading, done, data };
}