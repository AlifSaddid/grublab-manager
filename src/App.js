import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useState } from 'react';
import AppRoutes from './AppRoutes';
import UserContext from './context/UserContext';
import LoadingContext from './context/LoadingContext';
import Loading from './components/Loading';

function App() {

  const [user, setUser] = useState();

  const [loading, setLoading] = useState(false);

  const userContext = { user, setUser };

  const loadingContext = { loading, setLoading };

  return (
    <LoadingContext.Provider value={loadingContext}>
      <UserContext.Provider value={userContext}>
        <AppRoutes />
        <Loading display={loading} />
      </UserContext.Provider>
    </LoadingContext.Provider>
  );
}

export default App;
