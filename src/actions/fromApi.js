import ApiCall from './ApiCall';

export const HTTP_METHODS = {
    GET: "get",
    POST: "post",
    PUT: "put",
    DELETE: "delete",
};

class ApiCallActionCreator {

    login(username, password) {
        return ApiCall(
            HTTP_METHODS.POST,
            '/login',
            {
                username,
                password
            }
        );
    }

    getAllMenu() {
        return ApiCall(
            HTTP_METHODS.GET,
            '/v1/menus/'
        );
    }

}

const fromApi = new ApiCallActionCreator();

export default fromApi;