
const ApiCall = (method, url, payload) => (
    dispatch
) => {
    
    const body = payload && JSON.stringify(payload);
    
    const authorizationToken = localStorage.getItem('jwtToken');

    const api = 'http://localhost:8080';

    const headers = {
        Accept: 'application/json',
        'Content-Type': 'application/json',
    };

    if (authorizationToken) {
        headers.authorization = authorizationToken;
    }

    fetch(api + url, {
        headers: headers,
        method,
        body
    }).then((response) => {
        
        return response.text().then((rawBody) => {
            let body = rawBody;
            try {
                body = JSON.parse(rawBody);
            } catch(e) {

            }

            if (response.status === 401) {
                localStorage.clear();
                window.location.replace('/login');
            }

            if (response.headers.get('token')) {
                localStorage.setItem(
                    'jwtToken',
                    response.headers.get('token')
                );
            }

            console.log(body);

            return Promise.resolve(body);
        })
    })

}

export default ApiCall;