import React from 'react';
import { createUseStyles } from 'react-jss';

const useStyle = createUseStyles({
    container: {
        padding: "20px",
        width: "80%"
    }
})

export default function MainPanel(props) {
    const classes = useStyle();

    return (
        <div className={classes.container}>
            {props.children}
        </div>
    )

}