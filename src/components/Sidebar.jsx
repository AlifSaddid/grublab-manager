import React from 'react';
import { createUseStyles } from 'react-jss';
import { NavLink } from 'react-router-dom';

const useStyle = createUseStyles({
    container: {
        minWidth: "15%",
        position: "sticky",
        top: 0,
        height: "100vh",
        background: "#2a4158",
        color: "white",
        padding: 0
    },
    navLinks: {
        padding: 0,
        textAlign: "center",
    },
    navItem: {
        listStyle: "none",
        color: "white",
        padding: "20px 0"
    },
    active: {
        background: "#3a718c"
    }
});

export default function Sidebar(props) {

    const classes = useStyle();

    return (
        <div className={classes.container}>
                <ul className={classes.navLinks}>
                    <li className={`${classes.navItem} ${props.active === 'Home' && classes.active}`}>
                        <NavLink to="/">Home</NavLink>
                    </li>
                    <li className={`${classes.navItem} ${props.active === 'Manage Pengguna' && classes.active}`}>
                        <NavLink to="/users">Manage Pengguna</NavLink>
                    </li>
                    <li className={`${classes.navItem} ${props.active === 'Manage Menu' && classes.active}`}>
                        <NavLink to="/menus">Manage Menu</NavLink>
                    </li>
                    <li className={`${classes.navItem} ${props.active === 'History Pesanan' && classes.active}`}>
                        <NavLink to="/pesanan">History Pesanan</NavLink>
                    </li>
                </ul>
        </div>
    );

}