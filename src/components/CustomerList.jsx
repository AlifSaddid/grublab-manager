import axios from 'axios';
import React, { useContext, useEffect, useState } from 'react';
import { createUseStyles } from 'react-jss';
import { toaster } from 'evergreen-ui';
import LoadingContext from '../context/LoadingContext';

const useStyles = createUseStyles({
    tableContainer: {
        marginTop: "20px"
    },
    button: {
        marginLeft: "5px"
    }
});

export default function CustomerList(props) {
    const classes = useStyles();
    const [customers, setCustomers] = useState([]);
    const [editedCustomer, setEditedCustomer] = useState(0);
    const [isEditing, setIsEditing] = useState(false);
    const [newCustomer, setNewCustomer] = useState({});
    const { setLoading } = useContext(LoadingContext);

    const API = process.env.REACT_APP_API_URL;

    function cancelEdit() {
        setEditedCustomer(0);
        setIsEditing(false);
    }

    function saveEdit() {
        setLoading(true);
        axios.put(API + `/v1/customers/${editedCustomer}`, newCustomer, {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('jwtToken')
            }
        })
        .then(() => {
            toaster.success('Berhasil mengedit customer');
            props.setUpdate(!props.update);
        })
        .catch((e) => {
            toaster.danger('Gagal mengedit customer');
        })
        setLoading(false);
        setEditedCustomer(0);
        setIsEditing(false);
        setNewCustomer({});
    }

    useEffect(() => {
        setLoading(true);
        axios.get(API + '/v1/customers/', {}, {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('jwtToken')
            }
        })
        .then((response) => {
            setLoading(false);
            setCustomers(response.data);
        })
    }, [props.update, API, setLoading])

    return (
        <div className={classes.tableContainer}>
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">Line ID</th>
                        <th scope="col">Point(s)</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>

                <tbody>
                    {customers.map((customer) =>
                    isEditing ?
                    <tr>
                        {customer.idLine === editedCustomer ?
                        <>
                            <td>
                                <div type="text">{customer.idLine}</div>
                            </td>
                            <td>
                                <input 
                                    onChange={(event) => setNewCustomer({...newCustomer, point: event.target.value})}
                                    type="text" className="form-control" 
                                    value={newCustomer.point} />
                            </td>
                            <td>
                                <button onClick={() => saveEdit()} className={`btn btn-success ${classes.button}`}>Simpan</button>
                                <button onClick={() => cancelEdit()} className={`btn btn-danger ${classes.button}`}>Batal</button>
                            </td>
                        </>
                        :
                        <>
                            <td>{customer.idLine}</td>
                            <td>{customer.point}</td>
                        </>
                        }
                    </tr>
                    :
                    <tr>
                        <td>{customer.idLine}</td>
                        <td>{customer.point}</td>
                        <td>
                            <button onClick={() => {
                                setNewCustomer({
                                    idLine: customer.idLine,
                                    point: customer.point
                                })
                                setEditedCustomer(customer.idLine);
                                setIsEditing(true);
                            }} className={`btn btn-warning ${classes.button}`}>Edit</button>
                        </td>
                    </tr>
                    )}
                </tbody>
            </table>
        </div>
    )
}