import axios from 'axios';
import { toaster } from 'evergreen-ui';
import React, { useContext, useEffect, useState } from 'react';
import { createUseStyles } from 'react-jss';
import { useHistory } from 'react-router';
import LoadingContext from '../context/LoadingContext';

const useStyles = createUseStyles({
    tableContainer: {
        marginTop: "20px"
    },
    button: {
        marginLeft: "5px"
    }
});

export default function MenuList(props) {
    const history = useHistory();
    const classes = useStyles();
    const [menus, setMenus] = useState([]);
    const [editedMenu, setEditedMenu] = useState(0);
    const [isEditing, setIsEditing] = useState(false);
    const [newMenu, setNewMenu] = useState({});
    const { setLoading } = useContext(LoadingContext);

    const API = process.env.REACT_APP_API_URL;

    function handleDeleteMenu(id) {
        setLoading(true);
        axios.post(API + `/v1/menus/${id}`, {}, {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('jwtToken')
            }
        })
        .then(() => {
            setLoading(false);
            toaster.success('Berhasil menghapus menu');
            props.setUpdate(!props.update);
        })
        .catch((e) => {
            setLoading(false);
            toaster.danger('Gagal menghapus menu');
            if (e.response && e.response.status === 403) {
                history.push('/login');
            }
        })
    }

    function cancelEdit() {
        setEditedMenu(0);
        setIsEditing(false);
    }

    function saveEdit() {
        setLoading(true);
        axios.put(API + `/v1/menus/${editedMenu}`, newMenu, {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('jwtToken')
            }
        })
        .then(() => {
            toaster.success('Berhasil mengedit menu');
            props.setUpdate(!props.update);
        })
        .catch((e) => {
            toaster.danger('Gagal mengedit menu');
        });
        setLoading(false);
        setEditedMenu(0);
        setIsEditing(false);
        setNewMenu({});
    }

    useEffect(() => {
        setLoading(true);
        axios.get(API + '/v1/menus/', {}, {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('jwtToken')
            }
        })
        .then((response) => {
            setLoading(false);
            setMenus(response.data);
        })
    }, [props.update, API, setLoading])

    return (
        <div className={classes.tableContainer}>
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Description</th>
                        <th scope="col">Price</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    {menus.map((menu) => 
                        isEditing ?
                        <tr>
                            {menu.menuId === editedMenu ? 
                                <>
                                    <td><input onChange={(event) => setNewMenu({...newMenu, name: event.target.value})} type="text" className="form-control" value={newMenu.name} /></td>
                                    <td><input onChange={(event) => setNewMenu({...newMenu, description: event.target.value})} type="text" className="form-control" value={newMenu.description} /></td>
                                    <td><input onChange={(event) => setNewMenu({...newMenu, price: event.target.value})} type="text" className="form-control" value={newMenu.price}/></td>
                                    <td>
                                        <button onClick={() => saveEdit()} className={`btn btn-success ${classes.button}`}>Simpan</button>
                                        <button onClick={() => cancelEdit()} className={`btn btn-danger ${classes.button}`}>Batal</button>
                                    </td>
                                </>
                                :
                                <>
                                    <td>{menu.name}</td>
                                    <td>{menu.description}</td>
                                    <td>{menu.price}</td>
                                </>
                            }
                        </tr>
                        : 
                        <tr>
                            <td>{menu.name}</td>
                            <td>{menu.description}</td>
                            <td>{menu.price}</td>
                            <td>
                                <button onClick={() => {
                                    setNewMenu({
                                        name: menu.name,
                                        description: menu.description,
                                        price: menu.price
                                    })
                                    setEditedMenu(menu.menuId);
                                    setIsEditing(true);
                                }} className={`btn btn-warning ${classes.button}`}>Edit</button>
                                <button onClick={() => handleDeleteMenu(menu.menuId)} className={`btn btn-danger ${classes.button}`}>Hapus</button>
                            </td>
                        </tr>
                    )}
                </tbody>
            </table>
        </div>
    )

}