import React, { useEffect, useState } from 'react';
import Chart from 'react-google-charts';
import { createUseStyles } from 'react-jss';

const useStyle = createUseStyles({
    chartBox: {
        borderRadius: '8px',
        border: '1px solid #c4c4c4',
        padding: '10px',
        boxShadow:' 5px 5px #c4c4c4'
    }
})

export default function PenjualanChart(props) {
    const classes = useStyle();
    const [data, setData] = useState([]);

    useEffect(() => {
        const chartData = [
            ['Bulan', 'Pesanan'],
            ['Jan', 0],
            ['Feb', 0],
            ['Mar', 0],
            ['Apr', 0],
            ['May', 0],
            ['Jun', 0],
            ['Jul', 0],
            ['Aug', 0],
            ['Sep', 0],
            ['Oct', 0],
            ['Nov', 0],
            ['Des', 0],
        ];

        props.data.forEach((pesanan) => {
            const date = new Date(pesanan.date);
            const month = date.getMonth();
            chartData[month+1][1]++;
        });
        setData(chartData);
    }, [props.data, setData])

    return (
        <div className={classes.chartBox}>
            <p>Penjualan Per Bulan</p>
            <Chart 
                width='100%'
                height='30%'
                chartType='LineChart'
                data={data}
                options={{
                    vAxis: {
                        title: 'Pesanan'
                    },
                    legends: 'none'
                }}
            />
        </div>
    );

}