import { Combobox } from 'evergreen-ui';
import React, { useEffect, useState } from 'react';
import Chart from 'react-google-charts';
import { createUseStyles } from 'react-jss';

const useStyle = createUseStyles({
    chartBox: {
        borderRadius: '8px',
        border: '1px solid #c4c4c4',
        padding: '10px',
        boxShadow: '5px 5px #c4c4c4',
        marginTop: '20px'
    }
})

export default function MenuLineChart(props) {
    const classes = useStyle();

    const [data, setData] = useState({});
    const [menus, setMenus] = useState([]);
    const [choosedMenu, setChoosedMenu] = useState('');
    const [chartData, setChartData] = useState([]);

    useEffect(() => {
        const rawData = {};
        props.data.forEach((pesanan) => {
            pesanan.menus.forEach((menu) => {
                if (rawData[menu.name] === undefined) {
                    rawData[menu.name] = {}
                }
                const date = new Date(pesanan.date);
                const month = date.getMonth();
                if (rawData[menu.name][month] === undefined) {
                    rawData[menu.name][month] = 0;
                } else {
                    rawData[menu.name][month]++;
                }
            })
        })
        setData(rawData);
    }, [props.data]);

    useEffect(() => {
        const rawMenus = [];
        Object.keys(data).forEach((key) => {
            rawMenus.push(key);
        })
        setMenus(rawMenus);
    }, [data, setMenus]);

    useEffect(() => {
        if (menus.length > 0) {
            setChoosedMenu(menus[0]);
        }
    }, [menus, setChoosedMenu]);

    useEffect(() => {
        const rawChartData = [
            ['Bulan', 'Jumlah'],
            ['Jan', 0],
            ['Feb', 0],
            ['Mar', 0],
            ['Apr', 0],
            ['May', 0],
            ['Jun', 0],
            ['Jul', 0],
            ['Aug', 0],
            ['Sep', 0],
            ['Oct', 0],
            ['Nov', 0],
            ['Des', 0],
        ];

        if (data[choosedMenu]) {
            Object.keys(data[choosedMenu]).forEach((bulan) => {
                rawChartData[parseInt(bulan) + 1][1] = data[choosedMenu][bulan];
            });
        }

        setChartData(rawChartData);
    }, [choosedMenu, data, setChartData])

    return (
        <div className={classes.chartBox}>
            <p>Perkembangan Menu Per Bulan</p>
            {choosedMenu !== '' && 
                <Combobox
                    initialSelectedItem={{ label: choosedMenu }}
                    items={menus.reduce((acc, cur) => {
                        acc.push({
                            label: cur
                        })
                        return acc;
                    }, [])}
                    itemToString={item => (item ? item.label : '')}
                    onChange={(selected) => setChoosedMenu(selected.label)}
                />
            }
            <Chart 
                width='100%'
                height='100%'
                chartType='LineChart'
                data={chartData}
                options={{
                    vAxis: {
                        title: 'Pendapatan'
                    }
                }}
            />
        </div>
    );
}