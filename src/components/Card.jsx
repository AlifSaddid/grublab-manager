import React from 'react';
import { createUseStyles } from 'react-jss';

const useStyle = createUseStyles({
    cardContainer: {
        background: (props) => props.color,
        padding: "10px",
        width: "25%",
        color: "white",
        borderRadius: "10px",
        marginRight: "25px"
    }
})

export default function Card(props) {

    const classes = useStyle(props);

    return (
        <div className={classes.cardContainer}>
            <p>{props.text}</p>
            <h1>{props.number}</h1>
        </div>
    );

}