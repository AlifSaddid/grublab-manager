import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Container } from 'react-bootstrap';
import { createUseStyles } from 'react-jss';
import MenuChart from './MenuChart';
import PendapatanChart from './PendapatanChart';
import PenjualanChart from './PenjualanChart';
import MenuLineChart from './MenuLineChart';

const useStyle = createUseStyles({
    containerAtas: {
        display: "flex"
    },
    containerBawah: {
        paddingTop: "30px"
    },
    chartAtas: {
        marginBottom: '20px'
    }
});

export default function Statistics() {

    const classes = useStyle();

    const [data, setData] = useState([]);

    const API = process.env.REACT_APP_API_URL;

    useEffect(() => {
        axios.get(API + '/v1/orders/', {}, {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('jwtToken')
            }
        })
        .then((response) => {
            setData(response.data);
        })
    }, [API, setData]);

    return (
        <Container className={classes.containerBawah}>
            <h1>Statistik Penjualan</h1>
            <div className={`row ${classes.chartAtas}`}>
                <div className="col-8">
                    <PendapatanChart data={data} /> 
                </div>
                <div className="col-4">
                    <MenuChart data={data} />
                </div>
            </div>
            <PenjualanChart data={data} />
            <MenuLineChart data={data} />
        </Container>
    );
}