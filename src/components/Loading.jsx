import React from 'react';
import { createUseStyles } from 'react-jss';
import { BlockReserveLoading } from 'react-loadingg';

const useStyles = createUseStyles({
    modal: {
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        background: 'rgba(0, 0, 0, 0.6)',
        display: 'block'
    }
});

export default function Loading(props) {
    const classes = useStyles();

    return (
        props.display &&
        <div className={classes.modal}>
            <BlockReserveLoading />
        </div> 
    )
}