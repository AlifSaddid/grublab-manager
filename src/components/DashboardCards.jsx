import axios from 'axios';
import React, { useContext, useEffect, useState } from 'react';
import { Container } from 'react-bootstrap';
import { createUseStyles } from 'react-jss';
import LoadingContext from '../context/LoadingContext';
import Card from './Card';

const useStyle = createUseStyles({
    containerAtas: {
        display: "flex"
    }
});

export default function DashboardCard() {

    const [totalUsers, setTotalUsers] = useState(0);
    const [keuntungan, setKeuntungan] = useState(0);
    const [keuntunganBulanIni, setKeuntunganBulanIni] = useState(0);

    const { setLoading } = useContext(LoadingContext);

    const API = process.env.REACT_APP_API_URL;

    useEffect(() => {
        setLoading(true);
        fetch(API + '/v1/statistics/pendapatan/total/', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('jwtToken')
            }
        })
            .then((response) => response.json())
            .then((data) => {
                setKeuntungan(data);
            })
        setLoading(false)
    }, [setLoading, setKeuntungan, API])

    useEffect(() => {
        setLoading(true);
        fetch(API + '/v1/statistics/pendapatan/current-month/', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('jwtToken')
            }
        })
            .then((response) => response.json())
            .then((data) => {
                setKeuntunganBulanIni(data);
            })
        setLoading(false);
    }, [setLoading, setKeuntunganBulanIni, API]);

    useEffect(() => {
        setLoading(true);
        axios.get(API + '/v1/customers/')
        .then(res => {
            setTotalUsers(res.data.length);
        })
        setLoading(false);
    }, [setLoading, setTotalUsers, API]);

    const classes = useStyle();

    return (
        <Container className={classes.containerAtas}>
            <Card number={totalUsers} text="Pengguna" color="#00bfef"/>
            <Card number={keuntungan} text="Total Keuntungan" color="#00a65a"/>
            <Card number={keuntunganBulanIni} text="Keuntungan Bulan Ini" color="#0047b1"/>
        </Container>
    );
}