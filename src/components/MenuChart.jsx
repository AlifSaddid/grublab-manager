import React, { useEffect, useState } from 'react';
import Chart from 'react-google-charts';
import { createUseStyles } from 'react-jss';

const useStyle = createUseStyles({
    chartBox: {
        borderRadius: '8px',
        border: '1px solid #c4c4c4',
        padding: '10px',
        boxShadow:' 5px 5px #c4c4c4'
    }
})

export default function MenuChart(props) {
    const classes = useStyle();
    const [data, setData] = useState([]);
    const API = process.env.REACT_APP_API_URL;

    useEffect(() => {
        const rawData = {};
        props.data.forEach((pesanan) => {
            pesanan.menus.forEach((menu) => {
                rawData[menu.name] = (rawData[menu.name] === undefined ? 0 : rawData[menu.name] + 1);
            })
        })
        const cleanData = [];
        cleanData.push(["Menu", "Jumlah Terjual"])
        Object.keys(rawData).forEach((key) => {
            cleanData.push([key, rawData[key]]);
        });
        setData(cleanData);
    }, [API, setData, props.data]);

    return (
        <div className={classes.chartBox}>
            <p>Menu Terpopuler</p>
            <Chart 
                width='100%'
                height='100%'
                chartType='PieChart'
                loader={<div>Loading Chart</div>}
                data = {data} 
                options={{
                    legend: {
                        position: 'bottom',
                        textStyle: {fontSize: 6}
                    }
                }}
            />  
        </div>
    );
}