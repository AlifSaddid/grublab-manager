import React from 'react';
import { createUseStyles } from 'react-jss';

const useStyle = createUseStyles({
    container: {
        display: "flex",
        flexDirection: "row",
        width: "100%"
    }
});

export default function PageScaffold(props) {
    
    const classes = useStyle();

    return (
        <div className={classes.container}>
            {props.children}
        </div>
    );

}