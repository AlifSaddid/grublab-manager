import axios from 'axios';
import React, { useContext, useState } from 'react';
import { toaster } from 'evergreen-ui';
import LoadingContext from '../context/LoadingContext';

export default function AddMenuForm(props) {
    
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);

    const { setLoading } = useContext(LoadingContext);

    const API = process.env.REACT_APP_API_URL;

    function submitAddMenu(event) {
        event.preventDefault();
        if (name === '' || description === '' || price <= 0) {
            toaster.warning('Harap isi form dengan benar!');
            return ;
        }
        setLoading(true);
        axios.post(API + '/v1/menus/', {
            name,
            description,
            price
        }, {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('jwtToken')
            }
        }).then((response) => {
            setLoading(false);
            setName('');
            setDescription('');
            setPrice(0);
            toaster.success(`Menu ${name} berhasil ditambahkan!`);
            props.setUpdate(!props.update);
        }).catch((e) => {
            setLoading(false);
            console.log(e);
        })
    }

    return (
        <form>
            <div className="row">
                <div className="col-3">
                    <label htmlFor="name" className="form-label">Nama</label>
                    <input type="text" className="form-control" id="name" value={name} onChange={(event) => setName(event.target.value)}/>
                </div>
                <div className="col-4">
                    <label htmlFor="description" className="form-label">Deskripsi</label>
                    <input type="text" className="form-control" id="description" value={description} onChange={(event) => setDescription(event.target.value)}/>
                </div>
                <div className="col-3">
                    <label htmlFor="price" className="form-label">Harga</label>
                    <input type="number" className="form-control" id="price" value={price} onChange={(event) => setPrice(event.target.value)}/>
                </div>
                <div className="col-2">
                    <label htmlFor="add-menu" className="form-label">Tambah Menu</label>
                    <button className="btn btn-success" id="add-menu" type="submit" onClick={(event) => submitAddMenu(event)}>Submit</button>
                </div>
            </div>
        </form>
    );
}