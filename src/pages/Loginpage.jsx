import React, { useState, useContext } from 'react';
import { createUseStyles } from 'react-jss';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import { toaster } from 'evergreen-ui';
import UserContext from '../context/UserContext';
import LoadingContext from '../context/LoadingContext';

const useStyle = createUseStyles({
    loginForm: {
        width: "40%"
    },
    container: {
        height: "100vh",
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    }
});

export default function Loginpage() {

    const history = useHistory();
    const classes = useStyle();
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const { setUser } = useContext(UserContext);

    const { setLoading } = useContext(LoadingContext);

    const API = process.env.REACT_APP_API_URL;

    function postLogin(event) {
        event.preventDefault();
        if (username === '' || password === '') {
            toaster.warning('Form tidak boleh kosong!');
            return ;
        }
        setLoading(true);
        axios.post(API + '/login', {
            username,
            password
        })
        .then((response) => {
            if (response.status === 200) {
                toaster.success(`Selamat datang, ${username}!`);
                setUser(username);
                setLoading(false);
                localStorage.setItem(
                    'jwtToken',
                    response.headers.token
                );
                history.push('/');
            } else {
                setLoading(false);
                history.push('/login');
            }
        })
        .catch(() => {
            setLoading(false);
            history.push('/login');
            toaster.danger('Gagal login!');
        });
    }

    function handleOnChangeUsername(event) {
        setUsername(event.target.value);
    }

    function handleOnChangePassword(event) {
        setPassword(event.target.value);
    }

    return (
        <div className={`container ${classes.container}`}>
            <div className={classes.loginForm}>
                <form>
                    <div className="mb-3">
                        <label htmlFor="username" className="form-label">Username</label>
                        <input type="text" className="form-control" id="username" value={username} onChange={handleOnChangeUsername} required/>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="password" className="form-label">Password</label>
                        <input type="password" className="form-control" id="password" value={password} onChange={handleOnChangePassword} required/>
                    </div>
                    <button onClick={(event) => postLogin(event)} className="btn btn-success btn-block">Login</button>
                </form>
            </div>
        </div>
    )

}