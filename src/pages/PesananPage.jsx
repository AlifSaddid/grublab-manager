import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { createUseStyles } from 'react-jss';
import MainPanel from '../components/MainPanel';
import PageScaffold from '../components/PageScaffold';
import Sidebar from '../components/Sidebar';

const useStyle = createUseStyles({
    menu: {
        padding: '4px',
        margin: '3px',
        background: '#c4c4c4',
        borderRadius: '4px'
    }
})

export default function PesananPage() {
    const classes = useStyle();
    const API = process.env.REACT_APP_API_URL;

    const [data, setData] = useState([])

    useEffect(() => {
        axios.get(API + '/v1/orders/', {}, {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('jwtToken')
            }
        })
        .then((response) => {
            setData(response.data);
        })
    }, [API, setData]);

    return (
        <PageScaffold>
            <Sidebar active='History Pesanan' />
            <MainPanel>
                <h1>Daftar History Pesanan</h1>
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">Customer Name</th>
                            <th scope="col">Tanggal</th>
                            <th scope="col">Promo</th>
                            <th scope="col">Menu</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data?.map((pesanan) => (
                            <tr>
                                <td>{pesanan.displayName}</td>
                                <td>{pesanan.date}</td>
                                <td>{pesanan.promo}</td>
                                <td>{pesanan.menus.map((menu) => (
                                    <span className={classes.menu}>{menu.name}</span>
                                ))}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </MainPanel>
        </PageScaffold>
    )
}