import React, { useContext, useState } from 'react';
import PageScaffold from '../components/PageScaffold';
import Sidebar from '../components/Sidebar';
import MainPanel from '../components/MainPanel';
import { useHistory } from 'react-router';
import UserContext from '../context/UserContext';
import CustomerList from '../components/CustomerList';

export default function Userspage() {

    const history = useHistory();
    const { user } = useContext(UserContext);
    const [update, setUpdate] = useState(false);

    if (!user) {
        history.push('/login');
    }

    return (
        <PageScaffold>
            <Sidebar active='Manage Pengguna' />
            <MainPanel>
                <h1>Manage User</h1>
                <CustomerList update={update} setUpdate={setUpdate} />
            </MainPanel>
        </PageScaffold>
    );

}