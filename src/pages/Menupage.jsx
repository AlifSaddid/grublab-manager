import React, { useContext, useState } from 'react';
import PageScaffold from '../components/PageScaffold';
import Sidebar from '../components/Sidebar';
import MainPanel from '../components/MainPanel';
import { useHistory } from 'react-router';
import UserContext from '../context/UserContext';
import AddMenuForm from '../components/AddMenuForm';
import MenuList from '../components/MenuList';

export default function Menupage() {

    const history = useHistory();
    const { user } = useContext(UserContext);
    const [update, setUpdate] = useState(false);

    if (!user) {
        history.push('/login');
    }

    return (
        <PageScaffold>
            <Sidebar active='Manage Menu' />
            <MainPanel>
                <h1>Manage Menu</h1>
                <AddMenuForm update={update} setUpdate={setUpdate} />
                <MenuList update={update} setUpdate={setUpdate} />
            </MainPanel>
        </PageScaffold>
    );

}