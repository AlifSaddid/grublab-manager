import React, { useContext } from 'react';
import { Container } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import PageScaffold from '../components/PageScaffold';
import Sidebar from '../components/Sidebar';
import MainPanel from '../components/MainPanel';
import UserContext from '../context/UserContext';
import DashboardCard from '../components/DashboardCards';
import Statistics from '../components/Statistics';

export default function Homepage() {
    const history = useHistory();

    const { user } = useContext(UserContext);

    if (!user) {
        history.push('/login');
    }

    return (
        <PageScaffold>
            <Sidebar active='Home' />
            <MainPanel>
                <Container>
                    <DashboardCard />
                    <Statistics />
                </Container>
            </MainPanel>
        </PageScaffold>
    );

}