const initialState = {value: 1};

export default function reducer(prevState = initialState, action) {

    return {
        ...prevState,
        ...action.payload
    };

}